<?php
/**
 * This is the template for generating an action view file.
 * The following variables are available in this template:
 * - $this: the ControllerCode object
 * - $action: the action ID
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */

<?php
$label=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', basename($this->getControllerID()))))));
if($action==='index')
{
	echo "\$this->breadcrumbs=array(
	'$label',
);";
}
else
{
	$action=ucfirst($action);
	echo "\$this->breadcrumbs=array(
	'$label'=>array('/{$this->uniqueControllerID}'),
	'$action',
);";
}
?>

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Controlador "<?php echo '<?php'; ?> echo $this->id . '/' . $this->action->id; ?>"</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        
        <div class="widget-body-inner" style="display:block;">
            
            <div class="widget-main">

                <div class="row space-6"></div>
                
                <div>

                    <div id="resultado">
                        <div class="infoDialogBox">
                            <p>
                                Puedes Cambiar el contenido de esta página editando el archivo <b><?php echo '<?php'; ?> echo __FILE__; ?></b>.
                            </p>
                        </div>
                    </div>
                    
                    <div id="content-<?php echo '<?php'; ?> echo $this->id . '-' . $this->action->id; ?>">
                        Has Creado Exitosamente un Nuevo Controlador
                    </div>
                    
                </div>

            </div>

        </div>

    </div>

</div>
