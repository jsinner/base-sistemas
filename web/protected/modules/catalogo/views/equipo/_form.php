<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'articulo-form',
        'enableAjaxValidation' => false,
        #'id'=>'seccion-form',
        #'enableAjaxValidation' => false,
        #'enableClientValidation' => true,
        'clientOptions' => array(
            //'validateOnSubmit' => true,
            'validateOnType' => true,
            'validateOnChange' => true),
    ));
    ?>

    <div class="widget-box">
        <div id="resultadoRegistrar" class="infoDialogBox">
            <p>
                Por Favor Ingrese los datos correspondientes para registrar una sección, Los campos marcados con <span class="required">*</span> son requeridos.
            </p>
        </div>
        <div class="widget-header">
            <h4>Equipo</h4>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>

        </div>

        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">

                    <a href="#" class="search-button"></a>
                    <div style="display:block" class="search-form">
                        <div class="widget-main form">
                            <input type="hidden" id='id' name="id" value="<?php echo $model->id ?>" />
                            <?php
                            $equipo = TipoArticulo::model()->findAll(array('condition' => "nombre ILIKE 'EQUIPO'"));
                            $equipo = $equipo[0]['id'];
                            ?>
                            <input type="hidden" id='Articulo_tipo_articulo_id' name="Articulo_tipo_articulo_id" value="<?php echo $equipo; ?>" />

                            <div class="row">
                                <div class="col-md-6">
                                    Nombre del Equipo <span class="required">*</span>
                                    <br>
                                    <?php echo $form->textField($model, 'nombre', array('size' => 70, 'maxlength' => 100, 'class' => 'col-sm-12', 'id' => 'Articulo_nombre_form')); ?>
                                </div>
                                <div class="col-md-6">
                                    Unidad de Medida <span class="required">*</span>
                                    <br>
                                    <div id="unidadMedidaEquipo">
                                    <?php
                                    echo $form->dropDownList(
                                            $model, 'unidad_medida_id', CHtml::listData($unidadMedida, 'id', 'nombre'), array('class' => 'span-5', 'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7')
                                    );
                                    ?>
                                    </div>
                                    <div id="unidadMedidaUtencilio" class="hide">
                                    <?php
                                    echo $form->dropDownList(
                                            $model, 'unidad_medida_id', CHtml::listData($unidadMedidaUtencilio, 'id', 'nombre'), array('class' => 'span-5', 'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7')
                                    );
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    Precio Nacional <span class="required">*</span>
                                    <br>
                                    <?php echo $form->textField($model, 'precio_regulado', array('size' => 15, 'maxlength' => 15, 'class' => 'col-sm-12', 'id' => 'Articulo_precio_regulado_form')); ?>
                                </div>
                                <div class="col-md-4">
                                    Precio Baremo <span class="required">*</span>
                                    <br>
                                    <?php echo $form->textField($model, 'precio_baremo', array('size' => 15, 'maxlength' => 15, 'class' => 'col-sm-12', 'id' => 'Articulo_precio_baremo_form')); ?>
                                </div>
                                <div class="col-md-4">
                                    Unidad Monetaria
                                    <br>
                                    <b>
                                        <?php
                                        $unidadMonetariaCompuesta=$unidadMonetaria[0]['nombre'] . ' (' . $unidadMonetaria[0]['abreviatura'] . ')';?>
                                        <b>
                                            <?php echo CHtml::textField('Articulo_unidad_monetaria_form',$unidadMonetariaCompuesta,array('size'=>15, 'class'=>'col-sm-12', 'readOnly'=>'readOnly'));?>
                                        </b>
<!--                                        --><?php //echo $unidadMonetaria[0]['nombre'] . ' (' . $unidadMonetaria[0]['abreviatura'] . ')'; ?>
                                    </b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    Serial <span class="required">*</span>
                                    <b>
                                        <?php echo $form->textField($model,'serial',array('size'=>32, 'class'=>'col-sm-12', 'id'=>'Articulo_serial_form'));?>
                                    </b>
                                </div>
                                <div class="col-md-4">
                                    Meses de Vida Util <span class="required">*</span>
                                    <b>
                                        <?php echo $form->textField($model,'meses_vida_util', array('size'=>4, 'class'=>'col-sm-12', 'id'=>'Articulo_meses_vida_util_form')); ?>
                                    </b>
                                </div>

                                <div class="col-md-4">
                                    Marca <span class="required">*</span>
                                    <?php
                                    echo $form->dropDownList(
                                        $model, 'marca_id', CHtml::listData($marca, 'id', 'nombre'), array('class'=>'col-sm-12', 'empty' => array('' => '- SELECCIONE -'),'id'=>'Articulo_marca_id_form' )
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Presentacion <span class="required">*</span>
                                    <br>
                                    <b>
                                        <?php echo $form->textField($model,'presentacion',array('size'=>15, 'class'=>'col-sm-12', 'id'=>'Articulo_presentacion_form', 'style'=>'max-width:100%; min-width:100%;',));?>
                                    </b>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12 hidden" id="franja">
                                    Franja <span class="required">*</span>
                                    <br>
                                    <div id="cantidadFrajas"><?php // echo Fraja::model()->cantFranja(); ?></div>
                                    <?php
                                    echo $form->dropDownList(
                                            $model, 'franja_id', CHtml::listData(Franja::model()->findAll(), 'id', 'descripcion', 'nombre'  ), array('class' => 'span-5', 'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7')
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>


<script src="/public/js/modules/catalogo/equipo/equipo.js"></script>
<script src="/public/js/modules/catalogo/articulo.js"></script>

<?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/articulo.js', CClientScript::POS_END); ?>